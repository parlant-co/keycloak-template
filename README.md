# Keycloak Template

Here you can learn about writing a HTML/CSS template for Keycloak. (Hi Nils 👋)

## Woking locally

For that I made a simple setup for you to get going.

* Have [Docker](https://docker.com) installed

* Clone the repository somewhere comfortable

* Enter the directory and start the system

    ```console
    $ docker-compose up
    ```

    > Note: Starting everything for the first time might take a while at first.

* Now you can simply open the project in the editor of your choice.

* And the application will be running at http://localhost:8080/admin/master/console

    * After the first start, navigate to the site to change the [theme settings](http://localhost:8080/admin/master/console/#/realms/master/theme-settings)

    * Login using `admin` and `admin` as the user and password

    * Go to "Real Settings" > "Themes"

    * From the dropdown of the "Login Theme" choose "tailwind-example"

    * Click "Save" to save the setting

## Orientation

I pasted a simple tailwindcss example here.

Keycloak is a very powerful authentication, authorisation and identity provider.
It supports various authentication mechanisms, including openid-connect and federation with all
major identiy providers (Google, Microsoft, Github, and many more).

In this project, the folder `themes/tailwind-example` and `styles/` is of most interest.

> Note: What on earth is a `ftl`-file?
>
> This extension is based on the FreeMarker template language.
> It even has editor support. If you use VSCode, this extension might help https://marketplace.visualstudio.com/items?itemName=dcortes92.FreeMarker

### Existing themes

I have left a copy of the existing themes (which you can base off) in the `themes/` folder.
The default Keycloak themes include:

* base
* keycloak
* keycloak.v2

Checkout `base` as it has most pages modelled. Keep in mind, that we hardly need all of them.
Our main focus should be on E-Mail templates, Login, Password Recovery pages.

### Keycloak Themes

A Keycloak theme can be quite powerful, unfortunately, but understanding some of the basics will
get you going.

> Note: Read more on the offical documentation: https://www.keycloak.org/docs/11.0/server_development/index.html#_themes
>       You will not have read everything.

> Note: FreeMarker also has a vast documentation: https://freemarker.apache.org/docs/index.html
>       You will not have read everything.

> Note: I found this blog post article the most helpful: https://dev.to/trigo/how-to-customize-keycloak-themes-545

#### Layouts

```ftl
<#import "root.ftl" as layout>
<h1>Hi there</h1>
```

#### Variables

```ftl
<h1>${user.displayName}</h1>
```

#### HTML sanitization

This is quite important to know, FreeMarker does not sanitze user input by default (!).
Use the `kcSanitize(...)` template function for that.

```ftl
<p>${kcSanitize(userInput)}</p>
```

#### Translations

Keycloak has translations for all messages and content. Most of it is available on the `msg(...)` function.

```ftl
<p>${msg("socialLoginAlternative")}</p>
```

