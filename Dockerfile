ARG BASE_IMAGE="docker.io/bitnami/keycloak:18.0.2-debian-11-r3"
ARG BUILD_IMAGE="maven:3.8.6-openjdk-11-slim"

FROM ${BASE_IMAGE} AS development

FROM ${BUILD_IMAGE} AS buildstage

WORKDIR /app

COPY . .

RUN mvn package

